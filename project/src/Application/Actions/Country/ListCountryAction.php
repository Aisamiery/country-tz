<?php
declare(strict_types=1);

namespace App\Application\Actions\Country;

use Psr\Http\Message\ResponseInterface as Response;

class ListCountryAction extends CountryAction
{
    /**
     * @return Response
     * @throws \JsonException
     */
    protected function action(): Response
    {
        $iterator = null;
        $data = [];

        while ($keys = $this->redisService->scan($iterator, 'country.*')) {
            foreach ($keys as $key) {
                $country = explode('.', $key);
                $data[$country[1]] = $this->redisService->get($key);
            }
        }

        $this->response->getBody()->write(json_encode($data, JSON_THROW_ON_ERROR));
        return $this->response;
    }

}