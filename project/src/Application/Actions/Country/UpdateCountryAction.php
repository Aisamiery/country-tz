<?php
declare(strict_types=1);

namespace App\Application\Actions\Country;

use Psr\Http\Message\ResponseInterface as Response;
use Slim\Exception\HttpBadRequestException;

class UpdateCountryAction extends CountryAction
{
    /**
     * @return Response
     * @throws HttpBadRequestException
     */
    protected function action(): Response
    {
        // insert redis data
        $code = $this->resolveArg('countryCode');

        if (empty($code) || mb_strlen($code) > 2) {
            throw new HttpBadRequestException($this->request, 'Param country invalid');
        }

        $this->redisService->incr('country.' . $code);

        return $this->response;
    }

}