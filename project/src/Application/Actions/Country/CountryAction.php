<?php
declare(strict_types=1);

namespace App\Application\Actions\Country;

use App\Application\Actions\Action;
use Psr\Log\LoggerInterface;

abstract class CountryAction extends Action
{
    /** @var \Redis  */
    protected \Redis $redisService;

    public function __construct(LoggerInterface $logger, \Redis $redis)
    {
        $this->redisService = $redis;

        parent::__construct($logger);
    }
}